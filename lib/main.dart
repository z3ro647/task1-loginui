import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:task1_loginui/screens/otpScreen.dart';
import 'package:task1_loginui/screens/loginscreen1.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(GetMaterialApp(
    home: MeroApp(),
    routes: {
      '/screens/otpscreen':(BuildContext context) => OTPScreen(),
    },
  ));
}

class MeroApp extends StatelessWidget {
  const MeroApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Task 1 LoginUI',
      home: MeroHomePage(),
    );
  }
}

class MeroHomePage extends StatefulWidget {
  const MeroHomePage({Key? key}) : super(key: key);

  @override
  _MeroHomePageState createState() => _MeroHomePageState();
}

class _MeroHomePageState extends State<MeroHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoginScreen1()
    );
  }
}
