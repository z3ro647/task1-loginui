import 'package:flutter/material.dart';

class CustomSimpleDialog extends StatefulWidget {
  const CustomSimpleDialog({ Key? key }) : super(key: key);

  @override
  _CustomSimpleDialogState createState() => _CustomSimpleDialogState();
}

class _CustomSimpleDialogState extends State<CustomSimpleDialog> {

  var _selected = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Simple Dialog'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () { _displayDialog(context);},
              child: Text('Show SimpleDialog')
            ),
            SizedBox(
              height: 10.0,
            ),
            Text('Gender: $_selected')
          ],
        ),
      ),
    );
  }

  _displayDialog(BuildContext context) async {
    _selected = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return Expanded(
          child: SimpleDialog(
            title: Text('Choose Gender'),
            children:[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "Male"); },
                child: const Text('Male'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "Female");
                },
                child: const Text('Female'),
              ),
            ],
            elevation: 10,
            //backgroundColor: Colors.green,
          ),
        );
      },
    );
 
    if(_selected != null)
      {
        setState(() {
          _selected = _selected;
        });
      }
  }
}
