import 'package:flutter/material.dart';

class CustomCardView extends StatefulWidget {
  const CustomCardView({ Key? key }) : super(key: key);

  @override
  _CustomCardViewState createState() => _CustomCardViewState();
}

class _CustomCardViewState extends State<CustomCardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Card View'),
      ),
      body: (
        Center(
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ListTile(
                  leading: Icon(Icons.person, size: 100.0,),
                  title: Text('Monkey D. Luffy'),
                  subtitle: Text('Monkey D. Luffy'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(onPressed: () {}, child: Text('Click Here')),
                    TextButton(onPressed: () {}, child: Text('Chick Here'))
                  ],
                )
              ],
            ),
          ),
        )
      )
    );
  }
}