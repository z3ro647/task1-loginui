import 'package:flutter/material.dart';

enum WhyFarther { harder, smarter, selfStarter, tradingCharter }

class CustomPopupMenuButton extends StatefulWidget {
  const CustomPopupMenuButton({Key? key}) : super(key: key);

  @override
  _CustomPopupMenuButtonState createState() => _CustomPopupMenuButtonState();
}

class _CustomPopupMenuButtonState extends State<CustomPopupMenuButton> {
  late WhyFarther _selection;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Popup Menu Button'),
        ),
        body: Center(
          child: PopupMenuButton<WhyFarther>(
            onSelected: (WhyFarther result) {
              ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text('$result selected'))
              );
              print(result);
              setState(() {
                _selection = result;
              });
            },
            icon: Icon(Icons.add),
            itemBuilder: (BuildContext context) => <PopupMenuEntry<WhyFarther>>[
              const PopupMenuItem<WhyFarther>(
                value: WhyFarther.harder,
                child: Text('Working a lot harder'),
              ),
              const PopupMenuItem<WhyFarther>(
                value: WhyFarther.smarter,
                child: Text('Being a lot smarter'),
              ),
              const PopupMenuItem<WhyFarther>(
                value: WhyFarther.selfStarter,
                child: Text('Being a self-starter'),
              ),
              const PopupMenuItem<WhyFarther>(
                value: WhyFarther.tradingCharter,
                child: Text('Placed in charge of trading charter'),
              ),
            ],
          ),
        ));
  }
}
