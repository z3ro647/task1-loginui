import 'package:flutter/material.dart';

class CustomChip extends StatefulWidget {
  const CustomChip({ Key? key }) : super(key: key);

  @override
  _CustomChipState createState() => _CustomChipState();
}

class _CustomChipState extends State<CustomChip> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chip Widget'),
      ),
      body: Center(
        child: Chip(
          elevation: 20,
          padding: EdgeInsets.all(8),
          backgroundColor: Colors.greenAccent[100],
          shadowColor: Colors.black,
          avatar: CircleAvatar(
            backgroundImage: NetworkImage("https://lh3.googleusercontent.com/ogw/ADea4I4gwD1n886P_sb4S-6Qk5W0WP16x2edTYNM9jWb=s83-c-mo"),
          ),
          label: Text(
            'Vivek Kumar Gurung',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
    );
  }
}