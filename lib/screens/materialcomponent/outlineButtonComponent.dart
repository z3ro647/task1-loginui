import 'package:flutter/material.dart';

class CustomOutlineButton extends StatefulWidget {
  const CustomOutlineButton({ Key? key }) : super(key: key);

  @override
  _CustomOutlineButtonState createState() => _CustomOutlineButtonState();
}

class _CustomOutlineButtonState extends State<CustomOutlineButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Outline Button'),
      ),
      body: Center(
        child: OutlinedButton(
          onPressed: () {},
          child: Text('Chick Here'),
        ),
      ),
    );
  }
}