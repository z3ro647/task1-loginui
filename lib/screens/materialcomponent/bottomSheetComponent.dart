import 'package:flutter/material.dart';

class CustomBottmSheet extends StatefulWidget {
  const CustomBottmSheet({Key? key}) : super(key: key);

  @override
  _CustomBottmSheetState createState() => _CustomBottmSheetState();
}

class _CustomBottmSheetState extends State<CustomBottmSheet> {
  void _show(BuildContext ctx) {
    showModalBottomSheet(
        elevation: 10,
        backgroundColor: Colors.amber,
        context: ctx,
        builder: (ctx) => Container(
              width: 300,
              height: 250,
              color: Colors.white54,
              alignment: Alignment.center,
              child: Text('This is Bottom Sheet'),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bottom Sheet'),
      ),
      body: SafeArea(
        child: Center(
          child: ElevatedButton(
            onPressed: () => _show(context),
            child: Text('Show BottomSheet'),
          ),
        ),
      ),
    );
  }
}
