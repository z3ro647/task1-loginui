import 'package:flutter/material.dart';

class CustomToolTip extends StatefulWidget {
  const CustomToolTip({Key? key}) : super(key: key);

  @override
  _CustomToolTipState createState() => _CustomToolTipState();
}

class _CustomToolTipState extends State<CustomToolTip> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ToolTip'),
      ),
      body: Center(
          child: Tooltip(
        message: 'This is ToolTip',
        child: Text('Hover over the text to show a tooltip.'),
      )),
    );
  }
}
