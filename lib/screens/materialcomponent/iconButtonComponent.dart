import 'package:flutter/material.dart';

class CustomIconButton extends StatefulWidget {
  const CustomIconButton({ Key? key }) : super(key: key);

  @override
  _CustomIconButtonState createState() => _CustomIconButtonState();
}

class _CustomIconButtonState extends State<CustomIconButton> {
  double _volume = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Icon Button'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
          IconButton(
            icon: const Icon(Icons.volume_up),
            tooltip: 'Increase volume by 10',
            onPressed: () {
              if(_volume < 100) {
                setState(() {
                  _volume += 10;
                });
              }
            },
          ),
          Text('Volume : $_volume')
        ],
        ),
      ),
    );
  }
}