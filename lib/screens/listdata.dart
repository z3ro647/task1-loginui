import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

final colorGreen = HexColor("#09b976");

class ListData extends StatefulWidget {
  const ListData({ Key? key }) : super(key: key);

  @override
  _ListDataState createState() => _ListDataState();
}

class _ListDataState extends State<ListData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Transfer Recipt',
          style: TextStyle(
            fontWeight: FontWeight.normal
          ),
        ),
        backgroundColor: colorGreen,
        actions: [
          IconButton(onPressed: () { print('Search Call'); }, icon: Icon(Icons.search))
        ],
      ),
      body: ListView(
        children: <Widget>[
          DataTable(headingRowColor:
        MaterialStateColor.resolveWith((states) => HexColor('#f5f5f5')),
            columns: [
              DataColumn(label: Text(
                'Order No.',
                style: TextStyle(
                  fontSize: 13.0,
                  fontWeight: FontWeight.bold
                ),
                textAlign: TextAlign.center,
              )),
              DataColumn(
                label: Text(
                  'Form Location',
                  style: TextStyle(
                  fontSize: 13.0,
                    fontWeight: FontWeight.bold
                  ),
                )
              ),
              DataColumn(label: Text(
                'To Location',
                style: TextStyle(
                  fontSize: 13.0,
                  fontWeight: FontWeight.bold
                ),
                )
              ),
              
            ],
            rows: [
              DataRow(cells: [
                DataCell(
                  Text(
                    'TR0714806',
                    style: TextStyle(
                      fontSize: 13.0
                    ),
                  )
                ),
                DataCell(
                  Text(
                    'PRIMELINE',
                    style: TextStyle(
                      fontSize: 13.0
                    ),
                  )
                ),
                DataCell(
                  Text(
                    'S031',
                    style: TextStyle(
                      fontSize: 13.0
                    ),
                  )
                )
              ]),
              DataRow(cells: [
                DataCell(Text('TR072114', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),)),
              ]),
              DataRow(cells: [
                DataCell(Text('TR0721789', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),)),
              ]),
              DataRow(cells: [
                DataCell(Text('TR0721872', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ]),
              DataRow(cells: [
                DataCell(Text('TR724508', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S025', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ]),
              DataRow(cells: [
                DataCell(Text('TR0725268', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ]),
              DataRow(cells: [
                DataCell(Text('TR0725397', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ]),
              DataRow(cells: [
                DataCell(Text('TR0725398', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ]),
              DataRow(cells: [
                DataCell(Text('TR0725631', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ]),
              DataRow(cells: [
                DataCell(Text('TR0726451', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ]),
              DataRow(cells: [
                DataCell(Text('TR0726574', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('PRIMELINE', style: TextStyle(fontSize: 13.0),)),
                DataCell(Text('S031', style: TextStyle(fontSize: 13.0),))
              ])
            ],
          )
        ],
      )
    );
  }
}