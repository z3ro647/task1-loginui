import 'package:flutter/material.dart';
import 'package:task1_loginui/screens/materialcomponent/alertDialogComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/bottomNavigationBar.dart';
import 'package:task1_loginui/screens/materialcomponent/bottomSheetComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/cardViewComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/checkBoxComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/chipWidgetComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/circularProgressIndicatorComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/dataTableComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/dateTimePickerComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/dividerComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/drawercomponent.dart';
import 'package:task1_loginui/screens/materialcomponent/dropDownComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/elevatedButtonComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/expansionPanelComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/floatingActionButton.dart';
import 'package:task1_loginui/screens/materialcomponent/gridViewComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/iconButtonComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/linearProgressIndicatorComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/listTileComponenet.dart';
import 'package:task1_loginui/screens/materialcomponent/outlineButtonComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/popupMenuButtonComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/radioButtonComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/silverAppBarComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/simpleDialogComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/sliderButtonComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/snackBarComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/switchComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/tabBarComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/textButtonComponent.dart';
import 'package:task1_loginui/screens/materialcomponent/toolTipComponent.dart';

class MaterialPractice extends StatefulWidget {
  const MaterialPractice({Key? key}) : super(key: key);

  @override
  _MaterialPracticeState createState() => _MaterialPracticeState();
}

class _MaterialPracticeState extends State<MaterialPractice> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Material Component Practice'),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 10.0,
          ),
          Text(
            'App Structure and Navigation',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BottomNavBar()
                    )
                  );
                },
                child: Text('Bottom Navigation Bar'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomDrawer()
                    )
                  );
                },
                child: Text('Drawer'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomSilverAppBar()
                    )
                  );
                },
                child: Text('Silver App Bar'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomTabBar()
                    )
                  );
                },
                child: Text('Tab'),
              ),
            ),
          ),
          Text(
            'Buttons',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomDropDown()
                    )
                  );
                },
                child: Text('Dropdown Button'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomElevatedButton()
                    )
                  );
                },
                child: Text('Elevated Button'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomFloatingButton()
                    )
                  );
                },
                child: Text('Floating Action Button'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomIconButton()
                    )
                  );
                },
                child: Text('Icon Button'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomOutlineButton()
                    )
                  );
                },
                child: Text('Outlined Button'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomPopupMenuButton()
                    )
                  );
                },
                child: Text('Popup Menu Button'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomTextButton()
                    )
                  );
                },
                child: Text('Text Button'),
              ),
            ),
          ),
          Text(
            'Input and Selections',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomCheckBox()
                    )
                  );
                },
                child: Text('Check Box'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomDateTimePicker()
                    )
                  );
                },
                child: Text('Date & Time Pickers'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomRadioButton()
                    )
                  );
                },
                child: Text('Radio'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomSlider()
                    )
                  );
                },
                child: Text('Slider'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomSwitch()
                    )
                  );
                },
                child: Text('Switch'),
              ),
            ),
          ),
          Text(
            'Dialogs, alerts and panels',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomAlert()
                    )
                  );
                },
                child: Text('Alert Dialog'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomBottmSheet()
                    )
                  );
                },
                child: Text('Bottom Sheet'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomExpansionPanel()
                    )
                  );
                },
                child: Text('Expansion Panel'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomSimpleDialog()
                    )
                  );
                },
                child: Text('Simple Dialog'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomSnackBar()
                    )
                  );
                },
                child: Text('SnackBar'),
              ),
            ),
          ),
          Text(
            'Information Displays',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomCardView()
                    )
                  );
                },
                child: Text('Card'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomChip()
                    )
                  );
                },
                child: Text('Chip'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomCircularProgressIndicator()
                    )
                  );
                },
                child: Text('Circular Progress Indicator'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomDataTable()
                    )
                  );
                },
                child: Text('Data Table'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomGridView()
                    )
                  );
                },
                child: Text('Grid View'),
              ),
            ),
          ),
          
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomLinearProgressIndicator()
                    )
                  );
                },
                child: Text('Linear Progress Indicator'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomToolTip()
                    )
                  );
                },
                child: Text('Tooltip'),
              ),
            ),
          ),
          Text(
            'Layout',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomDivider()
                    )
                  );
                },
                child: Text('Divider'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
            child: Builder(
              builder: (context) => ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CustomListTile()
                    )
                  );
                },
                child: Text('List Tile'),
              ),
            ),
          ),
          // Padding(
          //   padding:
          //       const EdgeInsets.symmetric(vertical: 5.0, horizontal: 40.0),
          //   child: Builder(
          //     builder: (context) => ElevatedButton(
          //       onPressed: () {
          //         Navigator.push(
          //           context,
          //           MaterialPageRoute(
          //             builder: (context) => BottomNavBar()
          //           )
          //         );
          //       },
          //       child: Text('Stepper'),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
