import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

final colorGreen = HexColor("#09b976");

class RegisterPage1 extends StatefulWidget {
  const RegisterPage1({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage1> {
  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;

    final TextEditingController name = TextEditingController();
    TextEditingController email = TextEditingController();
    TextEditingController mobile = TextEditingController();
    TextEditingController password = TextEditingController();
    TextEditingController confirmpassword = TextEditingController();

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: ListView(
        children: [
          Container(
            height: deviceHeight * 0.35,
            child: Image.asset('assets/images/reward.jpg'),
          ),
          RegisterFormWidget(
            labeltext: "Name",
            controller: name,
            hinttext: 'Enter your Name',
            icon: Icons.person,
          ),
          RegisterFormWidget(
              labeltext: "Email",
              hinttext: "Enter your Email",
              controller: email,
              icon: Icons.email),
          RegisterFormWidget(
              labeltext: "Mobile Number",
              hinttext: "Enter your Mobile number",
              controller: mobile,
              icon: Icons.phone_android),
          RegisterFormWidget(
              labeltext: "Password",
              hinttext: "Enter your Password",
              controller: password,
              icon: Icons.lock),
          RegisterFormWidget(
              labeltext: "Confirm Password",
              hinttext: "Enter Confirm Password",
              controller: confirmpassword,
              icon: Icons.lock),
          SizedBox(
            height: 20.0,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 0.0, horizontal: 40.0),
            child: SizedBox(
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  String txtName = name.text.toString();
                  String txtEmail = email.text.toString();
                  String txtMobile = mobile.text.toString();
                  String txtPassword = password.text.toString();
                  String txtConfirmpassword = confirmpassword.text.toString();
                  print("Name is: $txtName");
                  formValidate(txtName, txtEmail, txtMobile, txtPassword,
                      txtConfirmpassword);
                },
                child: Text('Register'),
                style: ElevatedButton.styleFrom(
                  primary: colorGreen,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Already have an account?"),
              Builder(
                  builder: (context) => TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(color: colorGreen),
                      ))),
            ],
          ),
        ],
      ),
    );
  }
}

void formValidate(String txtName, String txtEmail, String txtMobile,
    String txtPassword, String txtConfirmpassword) {
  // if(txtName.length > 5) {
  //   print(txtName);
  //   print('Name is Short');
  // } else if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(txtEmail) == false) {
  //   print('Enter Valid Email');
  // } else {
  //   print('object');
  // }
  // var email = txtEmail.toString();
  // bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
  // print("Email is: $txtEmail");
  // print(emailValid);
}

// class RegisterFormWidget extends StatelessWidget {
//   const RegisterFormWidget({ Key? key, required this.labeltext, required this.hinttext, required this.controller, required this.icon }) : super(key: key);

//   final String labeltext;
//   final String hinttext;
//   final TextEditingController controller;
//   final IconData icon;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 40.0),
//       child: TextField(
//         controller: controller,
//         style: TextStyle(
//             color: colorGreen, fontWeight: FontWeight.bold, fontSize: 20.0),
//         decoration: InputDecoration(
//             labelText: labeltext,
//             labelStyle: TextStyle(color: Colors.grey),
//             hintText: hinttext,
//             hintStyle: TextStyle(color: colorGreen),
//             prefixIcon: Icon(
//               icon,
//               color: colorGreen,
//             ),
//             border: InputBorder.none),
//       ),
//     );
//   }
// }

class RegisterFormWidget extends StatefulWidget {
  const RegisterFormWidget(
      {Key? key,
      required this.labeltext,
      required this.hinttext,
      required this.controller,
      required this.icon})
      : super(key: key);

  final String labeltext;
  final String hinttext;
  final TextEditingController controller;
  final IconData icon;

  @override
  _RegisterFormWidgetState createState() => _RegisterFormWidgetState(
      labeltext: labeltext,
      hintlebel: hinttext,
      controller: controller,
      icon: icon);
}

class _RegisterFormWidgetState extends State<RegisterFormWidget> {
  _RegisterFormWidgetState(
      {required this.labeltext,
      required this.hintlebel,
      required this.controller,
      required this.icon});

  final String labeltext;
  final String hintlebel;
  final TextEditingController controller;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 40.0),
      child: TextField(
        controller: controller,
        style: TextStyle(
            color: colorGreen, fontWeight: FontWeight.bold, fontSize: 20.0),
        decoration: InputDecoration(
            labelText: labeltext,
            labelStyle: TextStyle(color: Colors.grey),
            hintText: hintlebel,
            hintStyle: TextStyle(color: colorGreen),
            prefixIcon: Icon(
              icon,
              color: colorGreen,
            ),
            border: InputBorder.none),
      ),
    );
  }
}
